/**
 * ShoppingCartWSSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.4  Built on : Dec 19, 2010 (08:18:42 CET)
 */
package es.upm.fi.sos.t2.shoppingcart;

import java.awt.List;
import java.rmi.RemoteException;
import java.util.Arrays;

import org.apache.axis2.AxisFault;

import es.upm.fi.sos.t3.shoppingcart.Budget;
import es.upm.fi.sos.t3.shoppingcart.CostOfCart;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import sun.print.resources.serviceui;

import es.upm.fi.sos.t3.shoppingcart.LoginResponse;
import es.upm.fi.sos.t3.shoppingcart.ProductAmountType;
import es.upm.fi.sos.t3.shoppingcart.ProductAvailable;
import es.upm.fi.sos.t3.shoppingcart.ProductAvailableUnits;
import es.upm.fi.sos.t3.shoppingcart.ProductPrice;
import es.upm.fi.sos.t3.shoppingcart.ProductsAmountsList;
import es.upm.fi.sos.y3.shoppingcartServiceStub.WarehouseInformationWSStub;
import es.upm.fi.sos.y3.shoppingcartServiceStub.WarehouseInformationWSStub.ProductName;
import es.upm.fi.sos.y3.shoppingcartServiceStub.WarehouseInformationWSStub.ProductsList;

/**
 * ShoppingCartWSSkeleton java skeleton for the axisService
 */
public class ShoppingCartWSSkeleton {

	private boolean login = true;
	private double presupuesto = 1000000;
	es.upm.fi.sos.t3.shoppingcart.ProductsAmountsList lista = new ProductsAmountsList();

	public boolean hasProduct(String productName) throws RemoteException {
		WarehouseInformationWSStub service = new WarehouseInformationWSStub();

		String[] ListaArray = service.getProductsList().getProduct();
		Arrays.sort(ListaArray);
		if (Arrays.binarySearch(ListaArray, productName) < 0) {
			return false;
		}

		return true;
	}

	public boolean checkSession() {
		return login;
	}

	double getPresupuesto() {
		return presupuesto;
	}

	void setPresupuesto(double value) {
		presupuesto = value;
	}

	int productValue(String producto) {
		int count = 0;
		for (ProductAmountType articulo : lista.getProductAmountInfo()) {
			if (articulo.getProduct().equals(producto)) {
				count += articulo.getAmount();
			}
		}
		return count;
	}

	int decreaseItemFromCart(String producto, int amount) {
		int count = 0;
		for (ProductAmountType articulo : lista.getProductAmountInfo()) {
			if (articulo.getProduct().equals(producto)) {
				count = articulo.getAmount() - amount;
				if (count < 0) count = 0;
				articulo.setAmount(count);
			}
		}
		return count;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param productName
	 * @throws ProductUnknownError
	 *             :
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.ProductAvailableUnits getProductAvailableUnits(
			es.upm.fi.sos.t3.shoppingcart.ProductName productName)
			throws ProductUnknownError, NotValidSessionError {

		WarehouseInformationWSStub service;

		ProductAvailableUnits disponibles = new ProductAvailableUnits();
		disponibles.setProductAvailableUnits(0);

		try {
			service = new WarehouseInformationWSStub();
			// Comprobamos que es una sesion valida.
			if (!checkSession()) {
				// Si no es sesion valida lanzo error.
				throw new NotValidSessionError();
			}
			// Comprobamos que existe el producto
			if (!(hasProduct(productName.getProductName()))) {
				throw new ProductUnknownError();
			}
			ProductName product = new ProductName();
			product.setProductName(productName.getProductName());
			disponibles = new ProductAvailableUnits();
			disponibles.setProductAvailableUnits(service
					.getProductAvailableUnits(product)
					.getProductAvailableUnits());

		} catch (AxisFault e) { // Error al crear el servicio.
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) { // Error al llamar al servicio.
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return disponibles;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @throws NotValidSessionError
	 *             :
	 */
	public es.upm.fi.sos.t3.shoppingcart.ProductsAmountsList productsInCart(
	) throws NotValidSessionError {
		//Comprobamos sesion
		if(!checkSession()){
			throw new NotValidSessionError();
		}
		return lista;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.Budget budget(

	) throws NotValidSessionError {
		if (!checkSession()) {
			throw new NotValidSessionError();
		}
		Budget resultado = new Budget();
		resultado.setBudget(presupuesto);
		return resultado;

	}

	/**
	 * Inicia sesión en el servicio.
	 * 
	 * @param login
	 */
	//Elevará la excepcion? Throws al final de la cabecera?
	public es.upm.fi.sos.t3.shoppingcart.LoginResponse login(
			es.upm.fi.sos.t3.shoppingcart.Login login) {
		
		LoginResponse loginRetorno = new LoginResponse();
		loginRetorno.setLoginResponse(true);
		
		if(!login.getUsername().equals("Jose") || !login.getUsername().equals("Pepe")){
			loginRetorno.setLoginResponse(false);
		}else if(!login.getPassword().equals("Esoj") || !login.getPassword().equals("Epep")){
			loginRetorno.setLoginResponse(false);
		}
		return loginRetorno;			
	}

	/**
	 * Auto generated method signature
	 * 
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.CostOfCart costOfCart(

	) throws NotValidSessionError {
		if(!checkSession()){
			throw new NotValidSessionError();
		}
		double count = 0;
		
		es.upm.fi.sos.t3.shoppingcart.ProductName aux = new es.upm.fi.sos.t3.shoppingcart.ProductName();
		for (ProductAmountType articulo : lista.getProductAmountInfo()) {
			aux.setProductName(articulo.getProduct());
			try {
				count += getProductPrice(aux).getProductPrice();
			} catch (ProductUnknownError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		CostOfCart resultado = new CostOfCart();
		resultado.setCostOfCart(count);
		return resultado;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.ProductsList getProductsList() 
			throws NotValidSessionError {
		// TODO : fill this with the necessary business logic
		if (!checkSession()) {
			throw new NotValidSessionError();
		}
		WarehouseInformationWSStub service;
		es.upm.fi.sos.t3.shoppingcart.ProductsList lista = new es.upm.fi.sos.t3.shoppingcart.ProductsList();
		try {
			service = new WarehouseInformationWSStub();
			ProductsList listaOrg = service.getProductsList();

			lista.setProduct(listaOrg.getProduct());
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lista;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param productAmount
	 * @throws ProductUnknownError
	 *             :
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.ProductAvailable checkProductAvailability(
			es.upm.fi.sos.t3.shoppingcart.ProductAmount productAmount)
			throws ProductUnknownError, NotValidSessionError {
		
		es.upm.fi.sos.t3.shoppingcart.ProductAvailable resultado = new ProductAvailable();
		resultado.setProductAvailable(false);
		// Comprobamos que es una sesion valida.
		if (!checkSession()) {
			// Si no es sesion valida lanzo error.
			throw new NotValidSessionError();
		}
		// Veo si existe el producto en el servidor.
		try {
			if (!(hasProduct(productAmount.getProductAmount().getProduct()))) {
				throw new ProductUnknownError();
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		es.upm.fi.sos.t3.shoppingcart.ProductName nombre = new es.upm.fi.sos.t3.shoppingcart.ProductName();
		nombre.setProductName(productAmount.getProductAmount().getProduct());
		if (getProductAvailableUnits(nombre).getProductAvailableUnits() < productAmount
				.getProductAmount().getAmount()) {
			resultado.setProductAvailable(true);
		}

		return resultado;

	}

	/**
	 * Auto generated method signature
	 * 
	 * @param productAmount4
	 * @throws ProductUnknownError
	 *             :
	 * @throws NotEnoughUnitsError
	 *             :
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.ProductAvailableUnits addToCart(
			es.upm.fi.sos.t3.shoppingcart.ProductAmount productAmount4)
			throws ProductUnknownError, NotEnoughUnitsError,
			NotValidSessionError {

		if (!checkSession()) {
			throw new NotValidSessionError();
		}

		// Comprobamos si existe el producto.
		// Comprobamos que existe el producto
		try {
			if (!(hasProduct(productAmount4.getProductAmount().getProduct()))) {
				throw new ProductUnknownError();
			}
			WarehouseInformationWSStub servicio = new WarehouseInformationWSStub();
			// Comprobamos si hay suficientes unidades en el almacen.
			ProductName producto = new ProductName();
			producto.setProductName(productAmount4.getProductAmount()
					.getProduct());
			if (servicio.getProductAvailableUnits(producto)
					.getProductAvailableUnits() < productAmount4
					.getProductAmount().getAmount()) {
				throw new NotEnoughUnitsError();
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Añadimos el producto al almacen
		lista.addProductAmountInfo(productAmount4.getProductAmount());

		// Return numero de elementos actuales en el carrito de este tipo.
		es.upm.fi.sos.t3.shoppingcart.ProductAvailableUnits resultado = new ProductAvailableUnits();
		resultado.setProductAvailableUnits(productValue(productAmount4
				.getProductAmount().getProduct()));
		return resultado;
	}

	/**
	 * Auto generated method signature
	 * 
	 */

	public void logout() {
		// TODO : fill this with the necessary business logic

	}

	/**
	 * Auto generated method signature
	 * 
	 * @throws NotValidSessionError
	 *             :
	 * @throws NotEnoughBudgetError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.Budget buy(

	) throws NotValidSessionError, NotEnoughBudgetError {
		
		// Comprobamos que es una sesion valida.
		if (!checkSession()) {
			// Si no es sesion valida lanzo error.
			throw new NotValidSessionError();
		}
		//Vemos si hay presupuesto suficiente.
		
		if(getPresupuesto()<costOfCart().getCostOfCart()){
			throw new NotEnoughBudgetError();
		}
		//Decrementamos el presupuesto del cliente.
		setPresupuesto(getPresupuesto()-costOfCart().getCostOfCart());
		//Vaciamos carro
		lista = new ProductsAmountsList();
		return budget();
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param productAmount9
	 * @throws ProductUnknownError
	 *             :
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.ProductAvailableUnits removeFromCart(
			es.upm.fi.sos.t3.shoppingcart.ProductAmount productAmount9)
			throws ProductUnknownError, NotValidSessionError {
		// Comprobamos sesion.
		if (!checkSession()) {
			throw new NotValidSessionError();
		}

		// Comprobamos si existe el producto.
		try {
			if (!(hasProduct(productAmount9.getProductAmount().getProduct()))) {
				throw new ProductUnknownError();
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Eliminamos n unidades del producto.
		int aux = decreaseItemFromCart(productAmount9.getProductAmount().getProduct(), productAmount9.getProductAmount().getAmount());
		ProductAvailableUnits resultado = new ProductAvailableUnits();
		resultado.setProductAvailableUnits(aux);
		
		// Return numero de elementos actuales en el carrito de este tipo.
		return resultado;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param productName11
	 * @throws ProductUnknownError
	 *             :
	 * @throws NotValidSessionError
	 *             :
	 */

	public es.upm.fi.sos.t3.shoppingcart.ProductPrice getProductPrice(
			es.upm.fi.sos.t3.shoppingcart.ProductName productName11)
			throws ProductUnknownError, NotValidSessionError {
		// TODO : fill this with the necessary business logic
		WarehouseInformationWSStub service;
		ProductPrice precio = new ProductPrice();
		precio.setProductPrice(0);
		try {
			service = new WarehouseInformationWSStub();
			// Comprobamos que es una sesion valida.
			if (!checkSession()) {
				// Si no es sesion valida lanzo error.
				throw new NotValidSessionError();
			}
			// Veo si existe el producto en el servidor.
			if (!(hasProduct(productName11.getProductName()))) {
				throw new ProductUnknownError();
			}

			// Devuelvo el precio del producto.
			ProductName test = new ProductName();
			test.setProductName(productName11.getProductName());

			precio.setProductPrice(service.getProductPrice(test)
					.getProductPrice());
			return precio;

		} catch (AxisFault e) { // Al generar la conexión.
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) { // Al llamar al metodo getProductsList
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return precio;
	}
}
